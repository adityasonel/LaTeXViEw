package com.heisen_berg.andlatex;

import android.app.Application;

import org.scilab.forge.jlatexmath.core.AjLatexMath;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AjLatexMath.init(this);
    }
}
