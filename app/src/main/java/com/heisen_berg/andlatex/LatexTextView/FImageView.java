package com.heisen_berg.andlatex.LatexTextView;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.widget.ImageView;

/**
 * Created by daquexian on 17-2-13.
 */

public class FImageView extends AppCompatImageView {
    public boolean centered;

    public FImageView(Context context) {
        super(context);
    }
}
