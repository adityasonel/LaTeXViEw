package com.heisen_berg.andlatex.LatexTextView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v7.widget.AppCompatTextView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.widget.TextView;

import org.scilab.forge.jlatexmath.core.AjLatexMath;
import org.scilab.forge.jlatexmath.core.Insets;
import org.scilab.forge.jlatexmath.core.TeXConstants;
import org.scilab.forge.jlatexmath.core.TeXFormula;
import org.scilab.forge.jlatexmath.core.TeXIcon;

import java.util.List;

public class LaTeXtView extends AppCompatTextView {
    public LaTeXtView(Context context) {
        super(context);
    }

    public void setTextWithFormula(TextWithFormula textWithFormula) {
        List<TextWithFormula.Formula> formulas = textWithFormula.getFormulas();
        final SpannableStringBuilder builder = textWithFormula;

        for (final TextWithFormula.Formula formula : formulas) {
            TeXFormula teXFormula = TeXFormula.getPartialTeXFormula(formula.content);

            Bitmap bitmap = getBitmap(teXFormula);
            if (bitmap.getWidth() > LatexTextView.MAX_IMAGE_WIDTH) {
                bitmap = Bitmap.createScaledBitmap(bitmap, LatexTextView.MAX_IMAGE_WIDTH,
                        bitmap.getHeight() * LatexTextView.MAX_IMAGE_WIDTH / bitmap.getWidth(),
                        false);
            }

            builder.setSpan(new CenteredImageSpan(getContext(), bitmap, 0), formula.start, formula.end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        setText(builder);
    }



    public Bitmap getBitmap(TeXFormula formula) {
        TeXIcon icon = formula.new TeXIconBuilder()
                .setStyle(TeXConstants.STYLE_DISPLAY)
                .setSize(18)
                .setWidth(TeXConstants.UNIT_SP, 18, TeXConstants.ALIGN_LEFT)
                .setIsMaxWidth(true)
                .setInterLineSpacing(TeXConstants.UNIT_SP, getPaint().getTextSize() / getPaint().density)
                .build();
        icon.setInsets(new Insets(5, 5, 5, 5));

        Bitmap image = Bitmap.createBitmap(icon.getIconWidth(), icon.getIconHeight(),
                Bitmap.Config.ARGB_4444);

        Canvas g2 = new Canvas(image);
        g2.drawColor(Color.TRANSPARENT);
        icon.paintIcon(g2, 0, 0);
        return image;
    }
}
